\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\thispagestyle {empty}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Preface}{7}{section*.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{9}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}History}{9}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Internet of Things}{10}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Privacy}{16}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Privacy by Design}{16}{section.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.5}Patterns}{16}{section.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.6}Pattern Template}{17}{section.1.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Privacy Patterns Catalogue}{19}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Anonymized Collector}{19}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Anonymized Disseminator}{22}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Numerical Abstractor}{23}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Identity Replacer}{25}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Location Abstractor}{26}{section.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.6}UI Builder}{27}{section.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.7}Gate Keeper}{28}{section.2.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.8}Ambient Consenter }{29}{section.2.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.9}Ambient Notifier}{30}{section.2.9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.10}Metadata Stripper}{31}{section.2.10}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.11}Dashboard}{32}{section.2.11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.12}Distributed Processor}{33}{section.2.12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.13}Distributed Storager}{34}{section.2.13}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.14}Broadcaster}{35}{section.2.14}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.15}Sufficient Sampler}{36}{section.2.15}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.16}Hierarchical Responsibility}{37}{section.2.16}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.17}Inferencer}{38}{section.2.17}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.18}Logger}{39}{section.2.18}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.19}Disclosure}{40}{section.2.19}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.20}Policy Builder}{41}{section.2.20}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.21}Chain Aggregator}{42}{section.2.21}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.22}Greedy Storage Cleaner}{43}{section.2.22}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.23}Consenter}{44}{section.2.23}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.24}Divide and Conquer}{45}{section.2.24}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.25}Negotiator}{46}{section.2.25}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.26}Lazy Acquisition}{47}{section.2.26}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.27}Preference Manager}{48}{section.2.27}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.28}Ownership Manager}{49}{section.2.28}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.29}Withdrawer}{50}{section.2.29}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.30}Image Analyser}{51}{section.2.30}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.31}Graceful Feature Degradation}{53}{section.2.31}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.32}Private Linker}{54}{section.2.32}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.33}Anonymous Communicator}{55}{section.2.33}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.34}Continuous Consent Acknowledger}{56}{section.2.34}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.35}Kill Switch}{57}{section.2.35}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.36}Policy Butler}{58}{section.2.36}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.37}Privacy Colour Coding}{59}{section.2.37}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.38}Privacy Icons}{60}{section.2.38}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.39}Greedy Silos}{61}{section.2.39}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.40}Breach Handler}{62}{section.2.40}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.41}Obfuscator}{62}{section.2.41}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.42}Zoning}{63}{section.2.42}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Design Space}{65}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}How to Use Design Patterns}{65}{section.3.1}
\contentsfinish 
